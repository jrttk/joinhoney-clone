// Feather Icon
feather.replace()


// Navigation

let hamburger = document.getElementById('hamburger')

hamburger.addEventListener('click', () => {
  let [mobileNav] = document.getElementsByClassName('mobile-nav-overlay')
  mobileNav.classList.contains('-active') ? mobileNav.classList.remove('-active') : mobileNav.classList.add('-active')
})

let closeBtn = document.getElementById('mobile-nav-close')

closeBtn.addEventListener('click', () => {
  let [mobileNav] = document.getElementsByClassName('mobile-nav-overlay')
  mobileNav.classList.remove('-active')
})